'use strict'

const Shipping = use('App/Models/Shipping')

/**
 * Resourceful controller for interacting with shippings
 */
class ShippingController {
  /**
   * Show a list of all shippings.
   * GET shippings
   */
  async index({
    request,
    response
  }) {
    var shippings = await Shipping
      .query()
      .fetch()

    return shippings
  }

  /**
   * Show a list of all shippings.
   * GET shippings
   */
  async calculate({
    request,
    response
  }) {

    const data = request.only(['distance', 'qtdAxis', 'loadTypeId'])

    var shippings = await Shipping
      .query()
      .where('initialDistanceKm', '<', data.distance)
      .where('finalDistanceKm', '>=', data.distance)
      .where('loadTypeId', '=', data.loadTypeId)
      .fetch()

    var shipping = shippings.rows[0].$attributes
    var priceAxis = parseFloat(shipping.valueKmAxis.replace(',', '.'))
    var value = data.distance * data.qtdAxis * priceAxis

    console.log(value)
    return value
  }

  /**
   * Render a form to be used for creating a new shipping.
   * GET shippings/create
   */
  async create({
    request
  }) {
    const data = request.only(['initialDistanceKm', 'finalDistanceKm', 'valueKmAxis', 'loadTypeId'])
    const shipping = await Shipping.create(data)

    return shipping
  }

  /**
   * Create/save a new shipping.
   * POST shippings
   */
  async store({
    request,
    response
  }) {}

  /**
   * Update shipping details.
   * PUT or PATCH shippings/:id
   */
  async update({
    params,
    request
  }) {
    const shipping = await Shipping.findOrFail(params.id)

    const data = request.only([
      'initialDistanceKm', 'finalDistanceKm', 'valueKmAxis', 'loadTypeId'
    ])

    shipping.merge(data)

    await shipping.save()

    return shipping
  }

  /**
   * Delete a shipping with id.
   * DELETE shippings/:id
   */
  async destroy({
    params,
    request,
    response
  }) {}
}

module.exports = ShippingController
