'use strict'

const LoadType = use('App/Models/LoadType')

class LoadTypeController {
    /**
   * Show a list of all shippings.
   * GET shippings
   */
  async index({
    request,
    response,
    view
  }) {
    var loadTypes = await LoadType
      .query()
      .fetch()

    return loadTypes
  }
}

module.exports = LoadTypeController
