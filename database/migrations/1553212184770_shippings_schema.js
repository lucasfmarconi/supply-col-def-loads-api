'use strict'

const Schema = use('Schema')

class ShippingsSchema extends Schema {
  up () {
    this.create('shippings', (table) => {
      table.increments()
      table.integer('initialDistanceKm')
      table.integer('finalDistanceKm')
      table.decimal('valueKmAxis', 6, 2)
      table.integer('loadTypeId').references('load_types.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('shippings')
  }
}

module.exports = ShippingsSchema
