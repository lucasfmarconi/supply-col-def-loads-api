FROM node:11-alpine

WORKDIR /app

COPY package*.json ./

ADD ".env.example" ".env"

COPY . .

RUN npm install sqlite3 --save
RUN npm install -g @adonisjs/cli

EXPOSE 3335

CMD [ "npm", "start" ]